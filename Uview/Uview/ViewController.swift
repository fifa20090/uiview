//
//  ViewController.swift
//  Uview
//
//  Created by Максим Бойко on 3/21/19.
//  Copyright © 2019 Максим Бойко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        pyramid(8)
       circle(6)
    }
    
    func pyramid (_ count : Int) {
        var width = 256
        var height = 256
        var x = 10
        var y = 200
        for i in 0...count {
            if i % 2 == 1 {
                drawRedBox(x: x, y: y, width: width, height: height)
            }
            else {
                drawBlueBox(x: x, y: y, width: width, height: height)
            }
            width = width - 20
            height = height - 20
            x = x + 10
            y = y + 10
        }
    }

    func drawRedBox(x: Int , y: Int,width :Int,height:Int) {
        let box = UIView.init(frame: CGRect(x: x, y: y, width: width, height: height))
        box.backgroundColor=UIColor.red
        view.addSubview(box)
    }
    
    func drawBlueBox(x: Int , y: Int,width :Int,height:Int) {
        let box = UIView.init(frame: CGRect(x: x, y: y, width: width, height: height))
        box.backgroundColor=UIColor.blue
        view.addSubview(box)
    }
    
    func circle (_ count : Int) {
        var radius = 140
        for i in 0...count {
            if i % 2 == 1 {
                drawRedCircle(radius: radius)
            }
            else {
                drawBlueCircle(radius: radius)
            }
           radius = radius - 20
        }
    }
    
    func drawRedCircle (radius: Int) {
        
        let box = UIBezierPath(arcCenter: CGPoint(x: 175, y: 225),radius: CGFloat(radius), startAngle: CGFloat(0),endAngle: CGFloat(Double.pi*2), clockwise: true)
         let shapeLayer = CAShapeLayer()
        shapeLayer.path=box.cgPath
         shapeLayer.fillColor = UIColor.red.cgColor
         view.layer.addSublayer(shapeLayer)
    }
    
    func drawBlueCircle(radius: Int) {
        let box = UIBezierPath(arcCenter: CGPoint(x: 175, y: 225),radius: CGFloat(radius), startAngle: CGFloat(0),endAngle: CGFloat(Double.pi*2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path=box.cgPath
        shapeLayer.fillColor = UIColor.blue.cgColor
        view.layer.addSublayer(shapeLayer)
    }
}

